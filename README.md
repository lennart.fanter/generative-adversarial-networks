# Generative Adversarial Networks

## Name
Generative Adversarial Networks Paper

## Description
This paper provides a basic and precise overview about Generative Adversarial Networks (GANs). After which one should get the key ideas as well as the key math behind GANs

## Usage
Use this knowledge for good.

## Authors and acknowledgment
Lennart Fanter

## License
All rights reserved and do not forget to cite.

## Project status
Currently writing
